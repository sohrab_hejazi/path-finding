
// 7View.h : interface of the CMy7View class
//

#pragma once


class CMy7View : public CView
{
protected: // create from serialization only
	CMy7View();
	DECLARE_DYNCREATE(CMy7View)

// Attributes
public:
	CMy7Doc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CMy7View();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
//	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPositionStop();
	afx_msg void OnNewSize3();
//	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnPositionStart();
	
	
	afx_msg void OnDesignAbstacle32781();
//	afx_msg void OnNcLButtonDown(UINT nHitTest, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnViewCheckbitmap();
	afx_msg void OnViewStatusBar();
	afx_msg void OnSearchA();
	void draw( int x,  int y,  int color);
	void draw_route(void);
	afx_msg void OnHeiristicmethodCrowflydistance();
	afx_msg void OnHeiristicmethodManhattanmethod();
	afx_msg void OnSearchT();
	int random_generator(int max);
	void make_abstacle(void);
	afx_msg void OnSearch4();
	void crow_fly_direct(void);
	void manhattan_base(void);
	void manhattan_direct(void);
	void create_lines(void);
	
	void RESET(void);
	afx_msg void OnFileSave();
	void clean_screen(void);
	afx_msg void OnEditGraphicoff();
};

#ifndef _DEBUG  // debug version in 7View.cpp
inline CMy7Doc* CMy7View::GetDocument() const
   { return reinterpret_cast<CMy7Doc*>(m_pDocument); }
#endif

