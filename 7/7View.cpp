
// 7View.cpp : implementation of the CMy7View class
//
#include <stdlib.h>
#include <stdio.h>
//#include <math.h>
#include <conio.h>
#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "7.h"
#endif
#define _USE_MATH_DEFINES // for C++
#include <cmath>
#include <string.h>
#include <time.h>
#include "7Doc.h"
#include "7View.h"
#include <windows.h>
#include <tchar.h>

//#include "close_list.h"
#include "heap.h"
#include "queue.h"
#include "heap_queue.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
int x,y,X1,X2,Y1,Y2,method=0;
float k=4,crow_base[1000],crow_direct[1000],manhtt_base[1000],manhtt_direct[1000];
int start=0, stop=0, abst=0,COUNTER=0,power=1000,graphic_off=0;
 int bitmap[1300][630]={0};
 int route_color=100,close_color=155,open_color=300;



// CMy7View

IMPLEMENT_DYNCREATE(CMy7View, CView)

BEGIN_MESSAGE_MAP(CMy7View, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CMy7View::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
//	ON_WM_RBUTTONUP()
	ON_COMMAND(ID_POSITION_STOP, &CMy7View::OnPositionStop)
	ON_COMMAND(ID_NEW_SIZE3, &CMy7View::OnNewSize3)
//	ON_WM_LBUTTONDOWN()
ON_WM_LBUTTONDOWN()
ON_COMMAND(ID_POSITION_START, &CMy7View::OnPositionStart)


ON_COMMAND(ID_DESIGN_ABSTACLE32781, &CMy7View::OnDesignAbstacle32781)
//ON_WM_NCLBUTTONDOWN()
ON_WM_KEYDOWN()
ON_COMMAND(ID_VIEW_CHECKBITMAP, &CMy7View::OnViewCheckbitmap)
ON_COMMAND(ID_VIEW_STATUS_BAR, &CMy7View::OnViewStatusBar)
ON_COMMAND(ID_SEARCH_A, &CMy7View::OnSearchA)
ON_COMMAND(ID_HEIRISTICMETHOD_CROWFLYDISTANCE, &CMy7View::OnHeiristicmethodCrowflydistance)
ON_COMMAND(ID_HEIRISTICMETHOD_MANHATTANMETHOD, &CMy7View::OnHeiristicmethodManhattanmethod)
ON_COMMAND(ID_SEARCH_T, &CMy7View::OnSearchT)
ON_COMMAND(ID_SEARCH_4, &CMy7View::OnSearch4)
ON_COMMAND(ID_FILE_SAVE, &CMy7View::OnFileSave)
ON_COMMAND(ID_EDIT_GRAPHICOFF, &CMy7View::OnEditGraphicoff)
END_MESSAGE_MAP()

// CMy7View construction/destruction

CMy7View::CMy7View()
{
	// TODO: add construction code here

}

CMy7View::~CMy7View()
{
}

BOOL CMy7View::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	
	return CView::PreCreateWindow(cs);
}



// CMy7View drawing

void CMy7View::OnDraw(CDC* pDC)
{
	CMy7Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	//pDC->Rectangle(100,100,50,50);
    //pDC->Ellipse(100,100,200,200);
	//pDC->SetDCPenColor(160);
	//pDC->SetBkColor(140);
	//pDC->SetTextColor(60);
	//pDC->SetDCBrushColor(80);
	//HANDLE handle= GetStdHandle(STD_OUTPUT_HANDLE);
    //SetConsoleTextAttribute( handle, 90);
	//pDC->SetColorAdjustment();
	float j;
	//if(ID_POSITION_START==true) k=4;
		
	
	if(k==1)
		return;
	j=630/k;
	j++;
	
	for(int i=0;i<j;i++)
	{
		pDC->MoveTo(0,i*k);
		pDC->LineTo(1300,i*k);
	}
	j=1300/k;
	j++;
	for(int i=0;i<j;i++)
	{
		pDC->MoveTo(i*k,0);
		pDC->LineTo(i*k,630);
	}
	//CPoint pos;
	POINT pos;
	/*int x=11000,y;
	do
	{x--;
     if(start==1)
	 {
	 GetCursorPos(&pos);
     x=pos.x;
     y=pos.y;
	 //pDC->LineTo(x,y);
	 pDC->SetPixel(pos,200);
	 start=0;
	 }
	} while(x>10); */
	
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CMy7View printing


void CMy7View::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CMy7View::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CMy7View::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CMy7View::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}

//void CMy7View::OnRButtonUp(UINT /* nFlags */, CPoint point)
//{
//	ClientToScreen(&point);
//	OnContextMenu(this, point);
//}

void CMy7View::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CMy7View diagnostics

#ifdef _DEBUG
void CMy7View::AssertValid() const
{
	CView::AssertValid();
}

void CMy7View::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMy7Doc* CMy7View::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMy7Doc)));
	return (CMy7Doc*)m_pDocument;
}
#endif //_DEBUG


// CMy7View message handlers


void CMy7View::OnPositionStop()
{
	// TODO: Add your command handler code here
	stop=1;
}


void CMy7View::OnNewSize3()
	
{
	/* TODO: Add your command handler code here
	k=3;
	CDC *x= new CDC;
	x->LineTo(100,300);	*/
	 
}


//void CMy7View::OnLButtonDown(UINT nFlags, CPoint point)
//{
//	// TODO: Add your message handler code here and/or call default
//	start=1;
//
//	CView::OnLButtonDown(nFlags, point);
//}

int CheckKeyState()
{  

   int P=GetKeyState(VK_CAPITAL);
    
   if (P & 0x0001)
      return(1);
   return 0 ;
  
}
	

void CMy7View::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	
	
	//CDC* pCDC = CDC::FromHandle();
	 //HDC desktop = GetDC(NULL);
	 //SetPixel(desktop,point.x,point.y,color);

	 //CDC *dc;
	//CDC *dc= new CDC;
    //dc->Attach( ::GetDC(NULL) );
	
	CClientDC dc(this);
	if(k==1)
	{  
		x=point.x; y=point.y;
		COLORREF color;
		if(start==1)
	    {
	         color = RGB(355,200,0);
			 X1=point.x; Y1=point.y;
		     //X1=point.x/k; Y1=point.y/k;
			 //X1=X1*k; Y1=Y1*k;
			 start=0;
	    }
	   else if(stop==1)
	   {
	       color = RGB(255,0,0);
		   X2=point.x; Y2=point.y;
	       //X2=point.x/k; Y2=point.y/k;
		   //X2=X2*k; Y2=Y2*k;
		   stop=0;
       }
	   else if(abst==1)
	   {
		   color = RGB(10,0,00);
		   bitmap[point.x][point.y]=1;
	   }
	   else
		   return;
	  dc.SetPixel(point.x,point.y,color);
	  return;
   }
	//X=point.x; Y=point.y;
	int z,h,x2,y2,xmap,ymap;
	xmap=point.x / k; ymap=point.y / k;
	x=xmap*k; y=ymap*k;
	z=x+k; h=y+k;x++;y++;
	
	//dc.SetPixel(point.x,point.y,color);
	if(start==1)
	{  X1=xmap; Y1=ymap;
	   COLORREF color = RGB(355,200,0);
       for(x;x<z;x++)
	   {   y2=y;
		   for(y2;y2<h;y2++)
			  dc.SetPixel(x,y2,color); 
	   }
	   start=0;
	}
	else if(stop==1)
	{   X2=xmap; Y2=ymap;
		 COLORREF color = RGB(255,0,0);
       for(x;x<z;x++)
	   {   y2=y;
		   for(y2;y2<h;y2++)
			  dc.SetPixel(x,y2,color); 
	   }
	   stop=0;
	}
	else if(abst==1)
	   {   bitmap[xmap][ymap]=1;
		   COLORREF color = RGB(10,0,00);
           for(x;x<z;x++)
	       {   y2=y;
	     	   for(y2;y2<h;y2++)
			   dc.SetPixel(x,y2,color); 
	       }
	      // CheckKeyState();
		   x--;
	   
       }

	  CView::OnLButtonDown(nFlags, point);
	
}


void CMy7View::OnPositionStart()
{
	// TODO: Add your command handler code here
	start=1;
}






void CMy7View::OnDesignAbstacle32781()
{
	// TODO: Add your command handler code here
	abst=1;
}


//void CMy7View::OnNcLButtonDown(UINT nHitTest, CPoint point)
//{
//	// TODO: Add your message handler code here and/or call default
//	
//
//	CView::OnNcLButtonDown(nHitTest, point);
//}


void CMy7View::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default
	if(abst!=1)
		return;
	if(k==1)
	{   
		CClientDC dc(this);
		if(nChar==37 && x>0)
	     x--;
	    else if(nChar==38 && y>0 )
	     y--; 
	    else if(nChar==39 && x<1300)
	     x++; 
		else if(nChar==40 && y<630)
	     y++; 
		else return;
		bitmap[x][y]=1;
		COLORREF color;
		color = RGB(10,0,00);
		bitmap[x][y]=1;
	    dc.SetPixel(x,y,color);
	    return;
    }
	if(nChar==37)
	{     if(x<k)
	      return;
		  int z,h,x2,y2;
	      x=x-k; 
	      x=x / k; y=y / k;
		  bitmap[x][y]=1;
	      x=x*k; y=y*k;
	      z=x+k; h=y+k;x++;y++;
	      CClientDC dc(this);
		  COLORREF color = RGB(10,0,00);
		  x2=x;
           for(x2;x2<z;x2++)
	       {   y2=y;
	     	   for(y2;y2<h;y2++)
			   dc.SetPixel(x2,y2,color); 
	       }
	}
	else if(nChar==38)
	{
		int z,h,x2,y2;
		  if(y<k)
	      return;
	      y=y-k; 
	      x=x / k; y=y / k;
		  bitmap[x][y]=1;
	      x=x*k; y=y*k;
	      z=x+k; h=y+k;x++;y++;
	      CClientDC dc(this);
		  COLORREF color = RGB(10,0,00);
		  x2=x;
           for(x2;x2<z;x2++)
	       {   y2=y;
	     	   for(y2;y2<h;y2++)
			   dc.SetPixel(x2,y2,color); 
	       }
	}
	else if(nChar==39)
	{
		int z,h,x2,y2;
		if(x>1289)
	     return;
	      x=x+k; 
	      x=x / k; y=y / k;
		  bitmap[x][y]=1;
	      x=x*k; y=y*k;
	      z=x+k; h=y+k;x++;y++;
	      CClientDC dc(this);
		  COLORREF color = RGB(10,0,00);
		  x2=x;
           for(x2;x2<z;x2++)
	       {   y2=y;
	     	   for(y2;y2<h;y2++)
			   dc.SetPixel(x2,y2,color); 
	       }
	}
	else if(nChar==40)
	{    if(y>619)
	     return;
		 int z,h,x2,y2;
	      y=y+k; 
	      x=x / k; y=y / k;
		  bitmap[x][y]=1;
	      x=x*k; y=y*k;
	      z=x+k; h=y+k;x++;y++;
	      CClientDC dc(this);
		  COLORREF color = RGB(10,0,00);
		  x2=x;
           for(x2;x2<z;x2++)
	       {   y2=y;
	     	   for(y2;y2<h;y2++)
			   dc.SetPixel(x2,y2,color); 
	       }
	};

	CView::OnKeyDown(nChar, nRepCnt, nFlags);
}



void CMy7View::OnViewCheckbitmap()
{  
	CClientDC dc(this);
	COLORREF color = RGB(255,255,255);
	for(int i=0; i<=1300;i++)
      for(int j=0; j<=630;j++)
		  dc.SetPixel(i,j,color);
	if(k==1)
	{   COLORREF color = RGB(10,0,00);
		for(int i=0; i<=1300;i++)
        for(int j=0; j<=630;j++)
	      if(bitmap[i][j]==1) dc.SetPixel(i,j,color);
		  
		return;
	}
	for(int i=0; i<=1300;i++)
      for(int j=0; j<=630;j++)
	  {
		  if(bitmap[i][j]==1)
		  {
			  int x,y,z,h,x2,y2;
	          x=i * k; y=j*k;
		      z=x+k; h=y+k;x++;y++;
	          CClientDC dc(this);
		      COLORREF color = RGB(10,0,00);
		      x2=x;
              for(x2;x2<z;x2++)
	          {   y2=y;
	     	      for(y2;y2<h;y2++)
			      dc.SetPixel(x2,y2,color); 
	          }

		  }
	  }

	// TODO: Add your command handler code here
}

/*int PrintNumber(__in double nNumber)
{
  _TCHAR szBuffer[100];
  
  _stprintf(szBuffer, _T("%i"), nNumber);
  //return MessageBox(hWnd, szBuffer, _T("wat"), MB_OK);
}*/

void CMy7View::OnViewStatusBar()
{
	// TODO: Add your command handler code here
	
}
 int close[100000][6],n;

void CMy7View::draw( int x,  int y,  int col)
{
	if(graphic_off)
		return;
	CClientDC dc(this);
	int d=100,s=400;
	if(col==route_color || col==10)
	{
		d=0;s=0;
	}

	if(k==1)
	{   COLORREF color = RGB(col,d,s);
		dc.SetPixel(x,y,color);
		  
		return;
	}
	
			  int z,h,x2,y2;
	          x=x * k; y=y*k;
			  //if((x==(X1*k) && y==(Y1*k)) || (x==(X2*k) && y==(Y2*k)))
					 //return;
			  if(x==(X1*k) && y==(Y1*k))
			  {
				  d=200;s=0;col=355;
			  }
			  if(x==(X2*k) && y==(Y2*k))
			  {
				  d=0;s=0;col=255;
			  }
		      z=x+k; h=y+k;x++;y++;
	          x2=x;
			  COLORREF color = RGB(col,d,s);
              for(x2;x2<z;x2++)
	          {   y2=y;
	     	      for(y2;y2<h;y2++)
			      dc.SetPixel(x2,y2,color); 
	          }
  
}

void CMy7View::OnSearchA()
{   
    n=0; 
	
	int  position[2], parent[2], gh[2],xp,yp;
	clock_t start, finish;
    double  duration;
	start = clock();
	heap_queue open;
	position[0]= X1; position[1]=Y1; parent[0]= X1; parent[1]= Y1; gh[0]= 0; gh[1]= 0;
	open.insert(position[0],position[1], parent[0],parent[1], gh[0],gh[1]);
	xp=X1; yp=Y1;
	bitmap[xp][yp]=3;
	while(1)
	{
		if(open.del())
		{   
			xp=open.kleinest[0]; yp=open.kleinest[1];
			
			for(int i=0; i<6; i++)
	          close[n][i]=open.kleinest[i];
			if(open.kleinest[0]==X2 && open.kleinest[1]==Y2)
			{
				draw_route();
				finish = clock();
	            duration = (double)(finish - start) / CLOCKS_PER_SEC;
				//duration=586954;
				LPCTSTR s;
				CString strText;
				//s.Format(TEXT("item %d"), i);
				//strText.InsertItem(5,s);
				//char kk=9;
				//printf( "%2.1f seconds\n", duration );
				_TCHAR szBuffer[100];
                 _stprintf(szBuffer, _T("%f"), duration);
				 if(!graphic_off)
				 MessageBox( szBuffer, _T("TIME"), MB_OK);
				 RESET();                
			    return;
			}
			bitmap[xp][yp]=2;
			draw(xp,yp,close_color);
			n++;
			for(int i=-1; i<2;i++)
				for(int j=-1; j<2;j++)
			    {   int g,m,h,G; 
						G=1.4*k;
                    if(bitmap[xp+i][yp+j] == 0 && xp+i<(1300/k) && yp+j<(630/k) && xp+i>=0 && yp+j>=0)
					{   
						int g,m,h,G; 
						G=1.4*k;

						//float distance;
						if(((xp+i) != xp) && ((yp+j) !=yp))
							g= G + close[n-1][4];
						else g= k + close[n-1][4]; 
						if(method==0)
						{
						     double question = (X2-(xp+i))*(X2-(xp+i))+(Y2-(yp+j))*(Y2-(yp+j));
						     h = sqrt(question)*k;
						}
						else
						{
						     h=abs(X2-(xp+i))+ abs(Y2-(yp+j));
						     h=h*k;
						}
						open.insert(xp+i,yp+j,xp,yp,g,h);
						bitmap[xp+i][yp+j] =3;
						draw(xp+i,yp+j,open_color);
					}
					else if(bitmap[xp+i][yp+j] == 3 )
					{   
						     
						    if(((xp+i) != xp) && ((yp+j) !=yp))
							g= G + close[n-1][4];
						    else g= k + close[n-1][4];
						    open.check(xp+i,yp+j,xp,yp,g);
                     
					}
			    } // end for i,j
		} //if open.del
		else
		{
            LPCTSTR s;
			CString strText;
			_TCHAR szBuffer[100];
             //if(!graphic_off)	 MessageBox( _T("unavailable"), _T("A * search"), MB_OK);
			RESET();
			return;

		} //else open.del
		
	}
	    
	
}





 

void CMy7View::draw_route(void)
{   
	int route[2000][6];
	for(int i=0; i<6; i++)
	route[0][i]=close[n][i];
	int count=0;
	n--;
	
		while(n>0)
		{
            if(close[n][0]==route[count][2] && close[n][1]==route[count][3])
			{
				 count++;
                 for(int i=0; i<6; i++)
	             route[count][i]=close[n][i];
				 if((route[count][0]==X1 && route[count][1]==Y1) || (route[count][0]==X2 && route[count][1]==Y2))
					 continue;
				 draw(route[count][0],route[count][1],route_color);
				 
			}
			n--;
		}
	
}


void CMy7View::OnHeiristicmethodCrowflydistance()
{
	// TODO: Add your command handler code here
	method=0;
}


void CMy7View::OnHeiristicmethodManhattanmethod()
{
	// TODO: Add your command handler code here
	method=1;
}

int CMy7View::random_generator(int max)
{
	
	
	return (rand()%max);
}


void CMy7View::OnSearchT()
{
	
   for(int counter=0;counter<5;counter++)
	{
	  
		
		n=0;
		int  position[2], parent[2], gh[2],xp,yp;
		clock_t start, finish;
	    double  duration;
		heap_queue open;
		srand( (unsigned)time( NULL ) );
		X1=position[0]= random_generator(1300/k);
		Y1=position[1]=random_generator(630/k);
		draw(X1,Y1,355);
		X2=parent[0]= random_generator(1300/k);
	    Y2=parent[1]= random_generator(630/k); gh[0]= 0; gh[1]= 0;
		draw(X2,Y2,255);
		open.insert(position[0],position[1], parent[0],parent[1], gh[0],gh[1]);
		xp=X1; yp=Y1;
		bitmap[xp][yp]=3;
		make_abstacle();
		start = clock();
	
	  while(1)
	  {   
		if(open.del())
		{   
			xp=open.kleinest[0]; yp=open.kleinest[1];
			
			for(int i=0; i<6; i++)
	          close[n][i]=open.kleinest[i];
			if(open.kleinest[0]==X2 && open.kleinest[1]==Y2)
			{
				finish = clock();
				draw_route();
				duration = (double)(finish - start) / CLOCKS_PER_SEC;
				LPCTSTR s;
				CString strText;
				_TCHAR szBuffer[100];
                _stprintf(szBuffer, _T("%f"), duration);
				if(!graphic_off)
				MessageBox( szBuffer, _T("TIME"), MB_OK);
				RESET();
				break;
			}
			bitmap[xp][yp]=2;
			draw(xp,yp,close_color);
			n++;
			for(int i=-1; i<2;i++)
				for(int j=-1; j<2;j++)
			    {   int g,m,h,G; 
						G=1.4*k;
                    if(bitmap[xp+i][yp+j] == 0 && xp+i<(1300/k) && yp+j<(630/k) && xp+i>=0 && yp+j>=0)
					{   
						int g,m,h,G; 
						G=1.4*k;
						if(((xp+i) != xp) && ((yp+j) !=yp))
						  g= G + close[n-1][4];
						else g= k + close[n-1][4]; 
						if(method==0)
						{
						     double question = (X2-(xp+i))*(X2-(xp+i))+(Y2-(yp+j))*(Y2-(yp+j));
						     h = sqrt(question)*k;
						}
						else
						{
						     h=abs(X2-(xp+i))+ abs(Y2-(yp+j));
						     h=h*k;
						}
						open.insert(xp+i,yp+j,xp,yp,g,h);
						bitmap[xp+i][yp+j] =3;
						draw(xp+i,yp+j,open_color);
					}
					else if(bitmap[xp+i][yp+j] == 3 )
					{   
						     
						    if(((xp+i) != xp) && ((yp+j) !=yp))
							g= G + close[n-1][4];
						    else g= k + close[n-1][4];
						    open.check(xp+i,yp+j,xp,yp,g);
                     
					}
			    } // end for i,j
		} //if open.del
		else
		{
                finish = clock();
				duration = (double)(finish - start) / CLOCKS_PER_SEC;
				LPCTSTR s;
				CString strText;
				_TCHAR szBuffer[100];
                _stprintf(szBuffer, _T("%f"), duration);
				//if(!graphic_off)				MessageBox( _T("unavailable"), _T("TIME"), MB_OK);
				RESET();
				counter--;
			break;
		} //else open.del
		
	  }//end while 1
	    
	} //end for 5
	

}




void CMy7View::make_abstacle(void)
{
	int x,y,div,z,zz=4;
	long int count,counter,limit;
	count=counter=0;
	limit=0.05*1300*630/(k*k);
	x= random_generator(1300/k);
	y= random_generator(630/k);
	z= random_generator(zz);
	div=random_generator(50)+20;
	while(counter<limit)
	{   
		int i,j;
		if(z==0)
		{
		 
			x=x-1;
			
		}
		else if(z==1 )
		{
		
		    x=x+1; 
			
		}
		else if(z==2)
		{
		 
		  y=y+1;
			
		}
		else if(z==3 )
		{
		
		     y=y-1;
			
		}
				
		if(bitmap[x][y]!=0) 	
			continue;
		if( (x==X1 && y==Y1) || (x==X2 && y==Y2) )
		{
			//x=x-i+1; y=y-j+1;	 
			x= random_generator(1300/k);
	        y= random_generator(630/k);
			z= random_generator(zz);
			div=random_generator(50)+20;
			count=0;
			continue;
		
		}
		if(x<0 || x>=(1300/k) || y<0 || y>=(630/k) )
		{
			x= random_generator(1300/k);
	        y= random_generator(630/k);	 
			z= random_generator(zz);
			div=random_generator(50)+20;
			count=0;
			continue;
		
		}
		
		counter++; count++;
		
		if(count%div==0)
		{
			count=0;
			x= random_generator(1300/k);
	        y= random_generator(630/k);
	        z= random_generator(zz);
			div=random_generator(55)+20;

		}
		bitmap[x][y]=1;
		draw(x,y,10);
			  
	}		  
}


void CMy7View::OnSearch4()
{
	// TODO: Add your command handler code here
	//int count=0;
	for(;COUNTER<50;COUNTER++)
	{
	  
		for(int i=0;i<1300;i++)
			for(int j=0;j<630;j++)
				bitmap[i][j]=0;

		n=0;
		int  position[2], parent[2], gh[2],xp,yp;
		clock_t start, finish;
	    double  duration;
		heap_queue open;
		srand( (unsigned)time( NULL ) );
		X1=position[0]= random_generator(1300/k);
		Y1=position[1]=random_generator(630/k);
		draw(X1,Y1,355);
		X2=parent[0]= random_generator(1300/k);
	    Y2=parent[1]= random_generator(630/k); gh[0]= 0; gh[1]= 0;
		draw(X2,Y2,255);
		open.insert(position[0],position[1], parent[0],parent[1], gh[0],gh[1]);
		xp=X1; yp=Y1;
		if(X1==X2 && Y1==Y2)
			continue;
		bitmap[xp][yp]=3;
		make_abstacle();
		start = clock();
	
	  while(1)
	  {   
		if(open.del())
		{   
			xp=open.kleinest[0]; yp=open.kleinest[1];
			
			for(int i=0; i<6; i++)
	          close[n][i]=open.kleinest[i];
			if(open.kleinest[0]==X2 && open.kleinest[1]==Y2)
			{
				finish = clock();
				draw_route();
				duration = (double)(finish - start) / CLOCKS_PER_SEC;
				duration*=power;

				LPCTSTR s;
				CString strText;
				_TCHAR szBuffer[100];
                _stprintf(szBuffer, _T("%f"), duration);
				 if(!graphic_off)
				 MessageBox( szBuffer, _T("TIME- base crow fly"), MB_OK);
				 crow_fly_direct();
				 manhattan_base();
				 manhattan_direct();
				 crow_base[COUNTER]=duration;
				 break;
			}
			bitmap[xp][yp]=2;
			draw(xp,yp,close_color);
			n++;
			for(int i=-1; i<2;i++)
				for(int j=-1; j<2;j++)
			    {   int g,m,h,G; 
						G=1.4*k;
                    if(bitmap[xp+i][yp+j] == 0 && xp+i<(1300/k) && yp+j<(630/k) && xp+i>=0 && yp+j>=0)
					{   
						int g,m,h,G; 
						G=1.4*k;
						if(((xp+i) != xp) && ((yp+j) !=yp))
						  g= G + close[n-1][4];
						else g= k + close[n-1][4]; 
						
						double question = (X2-(xp+i))*(X2-(xp+i))+(Y2-(yp+j))*(Y2-(yp+j));
						h = sqrt(question)*k;
						
						open.insert(xp+i,yp+j,xp,yp,g,h);
						bitmap[xp+i][yp+j] =3;
						draw(xp+i,yp+j,open_color);
					}
					else if(bitmap[xp+i][yp+j] == 3 )
					{   
						     
						    if(((xp+i) != xp) && ((yp+j) !=yp))
							g= G + close[n-1][4];
						    else g= k + close[n-1][4];
						    open.check(xp+i,yp+j,xp,yp,g);
                     
					}
			    } // end for i,j
		} //if open.del
		else
		{
                finish = clock();
				
				duration = (double)(finish - start) / CLOCKS_PER_SEC;
				LPCTSTR s;
				CString strText;
				_TCHAR szBuffer[100];
                _stprintf(szBuffer, _T("%f"), duration);
				//if(!graphic_off)			MessageBox( _T("unavailable"), _T("base crow fly"), MB_OK);
				RESET();
				COUNTER--;
			    break;
		} //else open.del
		
	  }//end while 1
	    
	} //end for 1000
	MessageBox( _T("process ist fertig"), _T(" "), MB_OK);
	RESET();

}


void CMy7View::crow_fly_direct(void)
{
	for(int i=0; i<1300;i++)
			    for(int j=0; j<630;j++)
					if(bitmap[i][j]!=1) bitmap[i][j]=0;
	clean_screen();
	create_lines();
	draw(X1,Y1,355);
	draw(X2,Y2,255);
	
	for(int i=0; i<=1300;i++)
      for(int j=0; j<=630;j++)
	  {
		  if(bitmap[i][j]==1)
		  {
			  int x,y,z,h,x2,y2;
	          x=i * k; y=j*k;
		      z=x+k; h=y+k;x++;y++;
	          CClientDC dc(this);
		      COLORREF color = RGB(10,0,00);
		      x2=x;
              for(x2;x2<z;x2++)
	          {   y2=y;
	     	      for(y2;y2<h;y2++)
			      dc.SetPixel(x2,y2,color); 
	          }

		  }
	  }
	n=0; 
	int  position[2], parent[2], gh[2],xp,yp;
	clock_t start, finish;
    double  duration;
	
	heap open;
	position[0]= X1; position[1]=Y1; parent[0]= X1; parent[1]= Y1; gh[0]= 0; gh[1]= 0;
	open.insert(position[0],position[1], parent[0],parent[1], gh[0],gh[1]);
	xp=X1; yp=Y1;
	bitmap[xp][yp]=3;
	start = clock();
	while(1)
	{
		if(open.del())
		{   
			xp=open.kleinest[0]; yp=open.kleinest[1];
			
			for(int i=0; i<6; i++)
	          close[n][i]=open.kleinest[i];
			if(open.kleinest[0]==X2 && open.kleinest[1]==Y2)
			{
				finish = clock();
				draw_route();
				duration = (double)(finish - start) / CLOCKS_PER_SEC;
				duration*=power;
				crow_direct[COUNTER]=duration;
				LPCTSTR s;
				CString strText;
				_TCHAR szBuffer[100];
                _stprintf(szBuffer, _T("%f"), duration);
				if(!graphic_off) 
				MessageBox( szBuffer, _T("TIME- direct crow fly"), MB_OK);
				
				clean_screen();
				create_lines();
				draw(X1,Y1,355);
	            draw(X2,Y2,255);
				//CMy7View.OnViewCheckbitmap;
	            return;
			}
			bitmap[xp][yp]=2;
			draw(xp,yp,close_color);
			n++;
			for(int i=-1; i<2;i++)
				for(int j=-1; j<2;j++)
			    {   int g,m,h,G; 
						G=1.4*k;
                    if(bitmap[xp+i][yp+j] == 0 && xp+i<(1300/k) && yp+j<(630/k) && xp+i>=0 && yp+j>=0)
					{   
						int g,m,h,G; 
						G=1.4*k;

						//float distance;
						if(((xp+i) != xp) && ((yp+j) !=yp))
							g= G + close[n-1][4];
						else g= k + close[n-1][4]; 
						
						     double question = (X2-(xp+i))*(X2-(xp+i))+(Y2-(yp+j))*(Y2-(yp+j));
						     h = sqrt(question)*k;
						
						open.insert(xp+i,yp+j,xp,yp,g,h);
						bitmap[xp+i][yp+j] =3;
						draw(xp+i,yp+j,open_color);
					}
					else if(bitmap[xp+i][yp+j] == 3 )
					{   
						     
						    if(((xp+i) != xp) && ((yp+j) !=yp))
							g= G + close[n-1][4];
						    else g= k + close[n-1][4];
						    open.check(xp+i,yp+j,xp,yp,g);
                     
					}
			    } // end for i,j
		} //if open.del
		else
		{
				finish = clock();
				
				duration = (double)(finish - start) / CLOCKS_PER_SEC;
				LPCTSTR s;
				CString strText;
				_TCHAR szBuffer[100];
                _stprintf(szBuffer, _T("%f"), duration);
				//if(!graphic_off)			MessageBox( _T("unavailable"), _T("direct crow fly"), MB_OK);
				
				clean_screen();
				create_lines();
				return;
		} //else open.del
		
	}
}


void CMy7View::manhattan_base(void)
{
	for(int i=0; i<1300;i++)
			    for(int j=0; j<630;j++)
					if(bitmap[i][j]!=1) bitmap[i][j]=0;
	/*CClientDC dc(this);
	COLORREF color = RGB(255,255,255);
	for(int i=0; i<=1300;i++)
      for(int j=0; j<=630;j++)
		  dc.SetPixel(i,j,color);*/
	
	for(int i=0; i<=1300;i++)
      for(int j=0; j<=630;j++)
	  {
		  if(bitmap[i][j]==1)
		  {
			  int x,y,z,h,x2,y2;
	          x=i * k; y=j*k;
		      z=x+k; h=y+k;x++;y++;
	          CClientDC dc(this);
		      COLORREF color = RGB(10,0,00);
		      x2=x;
              for(x2;x2<z;x2++)
	          {   y2=y;
	     	      for(y2;y2<h;y2++)
			      dc.SetPixel(x2,y2,color); 
	          }

		  }
	  }
	n=0; 
	int  position[2], parent[2], gh[2],xp,yp;
	clock_t start, finish;
    double  duration;
	
	heap_queue open;
	position[0]= X1; position[1]=Y1; parent[0]= X1; parent[1]= Y1; gh[0]= 0; gh[1]= 0;
	open.insert(position[0],position[1], parent[0],parent[1], gh[0],gh[1]);
	xp=X1; yp=Y1;
	bitmap[xp][yp]=3;
	start = clock();
	while(1)
	{
		if(open.del())
		{   
			xp=open.kleinest[0]; yp=open.kleinest[1];
			
			for(int i=0; i<6; i++)
	          close[n][i]=open.kleinest[i];
			if(open.kleinest[0]==X2 && open.kleinest[1]==Y2)
			{
				finish = clock();
				draw_route();
				duration = (double)(finish - start) / CLOCKS_PER_SEC;
				duration*=power;
				manhtt_base[COUNTER]=duration;
				LPCTSTR s;
				CString strText;
				_TCHAR szBuffer[100];
                _stprintf(szBuffer, _T("%f"), duration);
				if(!graphic_off) 
				MessageBox( szBuffer, _T("TIME- base Manhattan"), MB_OK);
				
				clean_screen();
				create_lines();
				draw(X1,Y1,355);
	            draw(X2,Y2,255);
				//CMy7View.OnViewCheckbitmap;
	            return;
			}
			bitmap[xp][yp]=2;
			draw(xp,yp,close_color);
			n++;
			for(int i=-1; i<2;i++)
				for(int j=-1; j<2;j++)
			    {   int g,m,h,G; 
						G=1.4*k;
                    if(bitmap[xp+i][yp+j] == 0 && xp+i<(1300/k) && yp+j<(630/k) && xp+i>=0 && yp+j>=0)
					{   
						int g,m,h,G; 
						G=1.4*k;

						//float distance;
						if(((xp+i) != xp) && ((yp+j) !=yp))
							g= G + close[n-1][4];
						else g= k + close[n-1][4]; 
						
						     h=abs(X2-(xp+i))+ abs(Y2-(yp+j));
						     h=h*k;
						
						open.insert(xp+i,yp+j,xp,yp,g,h);
						bitmap[xp+i][yp+j] =3;
						draw(xp+i,yp+j,open_color);
					}
					else if(bitmap[xp+i][yp+j] == 3 )
					{   
						     
						    if(((xp+i) != xp) && ((yp+j) !=yp))
							g= G + close[n-1][4];
						    else g= k + close[n-1][4];
						    open.check(xp+i,yp+j,xp,yp,g);
                     
					}
			    } // end for i,j
		} //if open.del
		else
		{
				finish = clock();
				
				duration = (double)(finish - start) / CLOCKS_PER_SEC;
				LPCTSTR s;
				CString strText;
				_TCHAR szBuffer[100];
                _stprintf(szBuffer, _T("%f"), duration);
				// MessageBox( _T("unavailable"), _T("base Manhattan"), MB_OK);
				
				clean_screen();
				create_lines();
				return;
		} //else open.del
		
	}
}


void CMy7View::manhattan_direct(void)
{
	for(int i=0; i<1300;i++)
			    for(int j=0; j<630;j++)
					if(bitmap[i][j]!=1) bitmap[i][j]=0;
	/*CClientDC dc(this);
	COLORREF color = RGB(255,255,255);
	for(int i=0; i<=1300;i++)
      for(int j=0; j<=630;j++)
		  dc.SetPixel(i,j,color);*/
	
	for(int i=0; i<=1300;i++)
      for(int j=0; j<=630;j++)
	  {
		  if(bitmap[i][j]==1)
		  {
			  int x,y,z,h,x2,y2;
	          x=i * k; y=j*k;
		      z=x+k; h=y+k;x++;y++;
	          CClientDC dc(this);
		      COLORREF color = RGB(10,0,00);
		      x2=x;
              for(x2;x2<z;x2++)
	          {   y2=y;
	     	      for(y2;y2<h;y2++)
			      dc.SetPixel(x2,y2,color); 
	          }

		  }
	  }
	n=0; 
	int  position[2], parent[2], gh[2],xp,yp;
	clock_t start, finish;
    double  duration;
	
	heap open;
	position[0]= X1; position[1]=Y1; parent[0]= X1; parent[1]= Y1; gh[0]= 0; gh[1]= 0;
	open.insert(position[0],position[1], parent[0],parent[1], gh[0],gh[1]);
	xp=X1; yp=Y1;
	bitmap[xp][yp]=3;
	start = clock();
	while(1)
	{
		if(open.del())
		{   
			xp=open.kleinest[0]; yp=open.kleinest[1];
			
			for(int i=0; i<6; i++)
	          close[n][i]=open.kleinest[i];
			if(open.kleinest[0]==X2 && open.kleinest[1]==Y2)
			{
				finish = clock();
				draw_route();
				duration = (double)(finish - start) / CLOCKS_PER_SEC;
				duration*=power;
				manhtt_direct[COUNTER]=duration;
				LPCTSTR s;
				CString strText;
				_TCHAR szBuffer[100];
                _stprintf(szBuffer, _T("%f"), duration);
				if(!graphic_off) 
				MessageBox( szBuffer, _T("TIME- direct Manhattan"), MB_OK);
				
				clean_screen();
				create_lines();
				//CMy7View.OnViewCheckbitmap;
	            return;
			}
			bitmap[xp][yp]=2;
			draw(xp,yp,close_color);
			n++;
			for(int i=-1; i<2;i++)
				for(int j=-1; j<2;j++)
			    {   int g,m,h,G; 
						G=1.4*k;
                    if(bitmap[xp+i][yp+j] == 0 && xp+i<(1300/k) && yp+j<(630/k) && xp+i>=0 && yp+j>=0)
					{   
						int g,m,h,G; 
						G=1.4*k;

						//float distance;
						if(((xp+i) != xp) && ((yp+j) !=yp))
							g= G + close[n-1][4];
						else g= k + close[n-1][4]; 
						
						     h=abs(X2-(xp+i))+ abs(Y2-(yp+j));
						     h=h*k;
						
						open.insert(xp+i,yp+j,xp,yp,g,h);
						bitmap[xp+i][yp+j] =3;
						draw(xp+i,yp+j,open_color);
					}
					else if(bitmap[xp+i][yp+j] == 3 )
					{   
						     
						    if(((xp+i) != xp) && ((yp+j) !=yp))
							g= G + close[n-1][4];
						    else g= k + close[n-1][4];
						    open.check(xp+i,yp+j,xp,yp,g);
                     
					}
			    } // end for i,j
		} //if open.del
		else
		{
				finish = clock();
				draw_route();
				duration = (double)(finish - start) / CLOCKS_PER_SEC;
				LPCTSTR s;
				CString strText;
				_TCHAR szBuffer[100];
                _stprintf(szBuffer, _T("%f"), duration);
				 //MessageBox( _T("unavailable"), _T("direct Manhattan"), MB_OK);
				
				clean_screen();
				create_lines();
				return;
		} //else open.del
		
	}
}


void CMy7View::create_lines(void)
{
	/*CDC* pDC;
	CMy7Doc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);*/
	CClientDC dc(this);
	float j;
	if(k==1)
		return;
	j=630/k;
	j++;
	
	for(int i=0;i<j;i++)
	{
		dc.MoveTo(0,i*k);
		dc.LineTo(1300,i*k);
	}
	j=1300/k;
	j++;
	for(int i=0;i<j;i++)
	{
		dc.MoveTo(i*k,0);
		dc.LineTo(i*k,630);
	}
}





void CMy7View::RESET(void)
{
	             for(int i=0;i<1300;i++)
		    	 for(int j=0;j<630;j++)
				 bitmap[i][j]=0;
 
		         n=0;
				 if(!graphic_off)
				 {
			     CClientDC dc(this);
			     COLORREF color = RGB(255,255,255);
			     for(int i=0; i<=1300;i++)
			     for(int j=0; j<=630;j++)
			     dc.SetPixel(i,j,color);
			     create_lines();
				 }
}


void CMy7View::OnFileSave()
{
	// TODO: Add your command handler code here
	float duration ;
    char str[10];
	FILE *fp1;
     fp1 = fopen("result.txt ","w"); 
     //fprintf(fp1,"crow_base ");	 fprintf(fp1,"crow_direct ");	 fprintf(fp1,"manhtt_base ");	 fprintf(fp1,"manhtt_direct\n");
	 for(int i=1; i<COUNTER; i++)
     {
    	duration=crow_base[i];
		if(duration<1 || duration>100)
			continue;
       	sprintf(str, "%.2f", duration);
    	fprintf(fp1,"%s ", str);
		
		duration=crow_direct[i];
       	sprintf(str, "%.2f", duration);
    	fprintf(fp1,"%s ", str);
		
		/////////////////////////////////
		duration=manhtt_base[i];
       	sprintf(str, "%.2f", duration);
    	fprintf(fp1,"%s ", str);
		
		duration=manhtt_direct[i];
       	sprintf(str, "%.2f", duration);
    	fprintf(fp1,"%s\n", str);
		
	
     }
     fclose(fp1);


/*	FILE *fp1;
      fp1 = fopen("crow_base.txt","w"); 
     
	 for(int i=0; i<COUNTER; i++)
     {
    	duration=crow_base[i];
       	sprintf(str, "%.2f", duration);
    	fprintf(fp1,"%s \n", str);
	
     }
     fclose(fp1);
////////////////////////////////////////////////////////////////

     FILE *fp2;
      fp2 = fopen("crow_direct.txt","w"); 
     
	 for(int i=0; i<COUNTER; i++)
     {
    	duration=crow_direct[i];
       	sprintf(str, "%.2f", duration);
    	fprintf(fp2,"%s \n", str);
	
     }
     fclose(fp2);
////////////////////////////////////////////////////////////////

	 FILE *fp3;
      fp3 = fopen("manhtt_base.txt","w"); 
     
	 for(int i=0; i<COUNTER; i++)
     {
    	duration=manhtt_base[i];
       	sprintf(str, "%.2f", duration);
    	fprintf(fp3,"%s \n", str);
	
     }
     fclose(fp3);
///////////////////////////////////////////////////////////////////

	 FILE *fp4;
      fp4 = fopen("manhtt_direct.txt","w"); 
     
	 for(int i=0; i<COUNTER; i++)
     {
    	duration=manhtt_direct[i];
       	sprintf(str, "%.2f", duration);
    	fprintf(fp4,"%s \n", str);
	
     }
     fclose(fp4);*/
	 
///////////////////////////////////////////////////////////////////
 MessageBox( _T("process ist fertig"), _T(" "), MB_OK);
return ;
}



void CMy7View::clean_screen(void)
{
	if(graphic_off)
		return;
	CClientDC dc(this);
	COLORREF color = RGB(255,255,255);
	for(int i=0; i<=1300;i++)
      for(int j=0; j<=630;j++)
		  dc.SetPixel(i,j,color);
}


void CMy7View::OnEditGraphicoff()
{
	// TODO: Add your command handler code here
	graphic_off=1;
}
