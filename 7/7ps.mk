
7ps.dll: dlldata.obj 7_p.obj 7_i.obj
	link /dll /out:7ps.dll /def:7ps.def /entry:DllMain dlldata.obj 7_p.obj 7_i.obj \
		kernel32.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \
.c.obj:
	cl /c /Ox /DREGISTER_PROXY_DLL \
		$<

clean:
	@del 7ps.dll
	@del 7ps.lib
	@del 7ps.exp
	@del dlldata.obj
	@del 7_p.obj
	@del 7_i.obj
